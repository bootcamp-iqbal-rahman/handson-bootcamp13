package id.co.nexsoft.house.model.occupant;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "occupant")
public class Occupant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "occcategory_id")
    @JsonBackReference
    private OccupantCategory occcategory_id;

    public Occupant() {}

    public Occupant(String name, String description, OccupantCategory occupantCategory) {
        this.name = name;
        this.description = description;
        this.occcategory_id = occupantCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OccupantCategory getOccupantCategory() {
        return occcategory_id;
    }

    public void setOccupantCategory(OccupantCategory occupantCategory) {
        this.occcategory_id = occupantCategory;
    }
}
