package id.co.nexsoft.house.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import id.co.nexsoft.house.model.building.Building;
import id.co.nexsoft.house.model.occupant.Status;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "house")
public class House {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "address")
    private String address;

    @Column(name = "total_floor")
    private int total_floor;

    @Column(name = "color")
    private String color;

    @OneToMany(mappedBy = "house_id", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Building> buildingList;

    @OneToMany(mappedBy = "house_id", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Status> occupatStatusList;

    public House() {
    }

    public House(String address, int total_floor, String color) {
        this.address = address;
        this.total_floor = total_floor;
        this.color = color;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTotal_floor() {
        return total_floor;
    }

    public void setTotal_floor(int total_floor) {
        this.total_floor = total_floor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
