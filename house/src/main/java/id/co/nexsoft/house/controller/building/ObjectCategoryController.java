package id.co.nexsoft.house.controller.building;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import id.co.nexsoft.house.model.building.ObjectCategory;
import id.co.nexsoft.house.repository.building.ObjectCategoryRepository;

@RestController
public class ObjectCategoryController {

    @Autowired
    private ObjectCategoryRepository objectCategoryRepository;

    @GetMapping("/objectCategory")
    public List<ObjectCategory> getAllData() {
        return objectCategoryRepository.findAll();
    }

    @GetMapping("/objectCategory/{id}")
    public ObjectCategory getDataById(@PathVariable int id) {
        return objectCategoryRepository.findById(id);
    }

    @PostMapping("/objectCategory")
    @ResponseStatus(HttpStatus.CREATED)
    public ObjectCategory addData(@RequestBody ObjectCategory objectCategory) {
        return objectCategoryRepository.save(objectCategory);
    }

    @DeleteMapping("/objectCategory/{id}")
    public void deleteData(@PathVariable int id) {
        objectCategoryRepository.deleteById(id);
    }

    @PutMapping("/objectCategory/{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody ObjectCategory objectCategory)
    {
        Optional<ObjectCategory> objectCategories
                = Optional.ofNullable(
                objectCategoryRepository.findById(id));

        if (!objectCategories.isPresent())
            return ResponseEntity
                    .notFound()
                    .build();

        objectCategory.setId(id);

        objectCategoryRepository.save(objectCategory);

        return ResponseEntity
                .noContent()
                .build();
    }
}
