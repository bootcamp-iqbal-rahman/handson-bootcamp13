package id.co.nexsoft.house.model.occupant;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "occupant_category")
public class OccupantCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "type_id")
    @JsonBackReference
    private Type type_id;

    @OneToMany(mappedBy = "occcategory_id", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Occupant> occupantList;

    public OccupantCategory() {}

    public OccupantCategory(String name, String description, Type occupantType) {
        this.name = name;
        this.description = description;
        this.type_id = occupantType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Type getOccupantType() {
        return type_id;
    }

    public void setOccupantType(Type occupantType) {
        this.type_id = occupantType;
    }
}
