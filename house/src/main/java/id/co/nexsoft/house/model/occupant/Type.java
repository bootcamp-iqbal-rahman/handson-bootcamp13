package id.co.nexsoft.house.model.occupant;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "type")
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "status_id")
    @JsonBackReference
    private Status status_id;

    @OneToMany(mappedBy = "type_id", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<OccupantCategory> occupantList;

    public Type() {}

    public Type(String name, String description, Status occupantStatus) {
        this.name = name;
        this.description = description;
        this.status_id = occupantStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getOccupantStatus() {
        return status_id;
    }

    public void setOccupantStatus(Status occupantStatus) {
        this.status_id = occupantStatus;
    }
}
