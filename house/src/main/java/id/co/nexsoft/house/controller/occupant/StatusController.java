package id.co.nexsoft.house.controller.occupant;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PutMapping;

import id.co.nexsoft.house.model.occupant.Status;
import id.co.nexsoft.house.repository.occupant.StatusRepository;

@RestController
public class StatusController {
    
    @Autowired
    private StatusRepository occupantStatusRepository;

    @GetMapping("/status")
    public List<Status> getAllData() {
        return occupantStatusRepository.findAll();
    }

    @GetMapping("/status/{id}")
    public Status getDataById(@PathVariable int id) {
        return occupantStatusRepository.findById(id);
    }

    @PostMapping("/status")
    @ResponseStatus(HttpStatus.CREATED)
    public Status addData(@RequestBody Status occupantStatus) {
        return occupantStatusRepository.save(occupantStatus);
    }

    @DeleteMapping("/status/{id}")
    public void deleteData(@PathVariable int id) {
        occupantStatusRepository.deleteById(id);
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id, 
        @RequestBody Status occupantStatus) 
    {
        Optional<Status> occupantStatuss
            = Optional.ofNullable(
                occupantStatusRepository.findById(id));

        if (!occupantStatuss.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        occupantStatus.setId(id);

        occupantStatusRepository.save(occupantStatus);

        return ResponseEntity
            .noContent()
            .build();
    }
}
