package id.co.nexsoft.house.repository.building;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.building.ObjectCategory;

public interface ObjectCategoryRepository extends CrudRepository<ObjectCategory, Integer> {
    ObjectCategory findById(int id);
    List<ObjectCategory> findAll();
    void deleteById(int id);
}
