package id.co.nexsoft.house.repository.occupant;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.occupant.Type;

import java.util.List;

public interface TypeRepository extends CrudRepository<Type, Integer> {
    Type findById(int id);
    List<Type> findAll();
    void deleteById(int id);
}