package id.co.nexsoft.house.repository.occupant;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.occupant.Status;
import java.util.List;


public interface StatusRepository extends CrudRepository<Status, Integer> {
    Status findById(int id);
    List<Status> findAll();
    void deleteById(int id);
}