package id.co.nexsoft.house.controller.building;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import id.co.nexsoft.house.model.building.Building;
import id.co.nexsoft.house.repository.building.BuildingRepository;

@RestController
public class BuildingController {

    @Autowired
    private BuildingRepository buildingRepository;

    @GetMapping("/building")
    public List<Building> getAllData() {
        return buildingRepository.findAll();
    }

    @GetMapping("/building/{id}")
    public Building getDataById(@PathVariable int id) {
        return buildingRepository.findById(id);
    }

    @PostMapping("/building")
    @ResponseStatus(HttpStatus.CREATED)
    public Building addData(@RequestBody Building building) {
        return buildingRepository.save(building);
    }

    @DeleteMapping("/building/{id}")
    public void deleteData(@PathVariable int id) {
        buildingRepository.deleteById(id);
    }

    @PutMapping("/building/{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody Building building)
    {
        Optional<Building> buildings
                = Optional.ofNullable(
                buildingRepository.findById(id));

        if (!buildings.isPresent())
            return ResponseEntity
                    .notFound()
                    .build();

        building.setId(id);

        buildingRepository.save(building);

        return ResponseEntity
                .noContent()
                .build();
    }
}
