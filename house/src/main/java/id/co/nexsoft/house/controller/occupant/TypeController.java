package id.co.nexsoft.house.controller.occupant;

import id.co.nexsoft.house.model.occupant.Type;
import id.co.nexsoft.house.repository.occupant.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TypeController {

    @Autowired
    private TypeRepository occupantTypeRepository;

    @GetMapping("/type")
    public List<Type> getAllData() {
        return occupantTypeRepository.findAll();
    }

    @GetMapping("/type{id}")
    public Type getDataById(@PathVariable int id) {
        return occupantTypeRepository.findById(id);
    }

    @PostMapping("/type")
    @ResponseStatus(HttpStatus.CREATED)
    public Type addData(@RequestBody Type occupantType) {
        return occupantTypeRepository.save(occupantType);
    }

    @DeleteMapping("/type{id}")
    public void deleteData(@PathVariable int id) {
        occupantTypeRepository.deleteById(id);
    }

    @PutMapping("/type{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody Type occupantType) {
        Optional<Type> occupantTypes = Optional.ofNullable(occupantTypeRepository.findById(id));

        if (!occupantTypes.isPresent())
            return ResponseEntity.notFound().build();

        occupantType.setId(id);

        occupantTypeRepository.save(occupantType);

        return ResponseEntity.noContent().build();
    }
}
