package id.co.nexsoft.house.controller.building;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import id.co.nexsoft.house.model.building.Object;
import id.co.nexsoft.house.repository.building.ObjectRepository;

import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class ObjectController {

    @Autowired
    private ObjectRepository objectRepository;
    
    @GetMapping("/object")
    public List<Object> getAllData() {
        return objectRepository.findAll();
    }

    @GetMapping("/object/{id}")
    public Object getDataById(@PathVariable int id) {
        return objectRepository.findById(id);
    }

    @PostMapping("/object")
    @ResponseStatus(HttpStatus.CREATED)
    public Object addData(@RequestBody Object object) {
        return objectRepository.save(object);
    }

    @DeleteMapping("/object/{id}")
    public void deleteData(@PathVariable int id) {
        objectRepository.deleteById(id);
    }

    @PutMapping("/object/{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody Object object)
    {
        Optional<Object> objects
                = Optional.ofNullable(
                objectRepository.findById(id));

        if (!objects.isPresent())
            return ResponseEntity
                    .notFound()
                    .build();

        object.setId(id);

        objectRepository.save(object);

        return ResponseEntity
                .noContent()
                .build();
    }
}
