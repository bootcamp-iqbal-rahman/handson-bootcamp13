package id.co.nexsoft.house.repository.building;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.building.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {
    Room findById(int id);
    List<Room> findAll();
    void deleteById(int id);
}


