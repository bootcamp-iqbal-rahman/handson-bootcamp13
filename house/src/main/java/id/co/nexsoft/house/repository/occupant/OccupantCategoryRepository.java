package id.co.nexsoft.house.repository.occupant;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.occupant.OccupantCategory;

import java.util.List;

public interface OccupantCategoryRepository extends CrudRepository<OccupantCategory, Integer> {
    OccupantCategory findById(int id);
    List<OccupantCategory> findAll();
    void deleteById(int id);
}
