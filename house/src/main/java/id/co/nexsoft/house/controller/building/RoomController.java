package id.co.nexsoft.house.controller.building;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import id.co.nexsoft.house.model.building.Room;
import id.co.nexsoft.house.repository.building.RoomRepository;

@RestController
public class RoomController {

    @Autowired
    private RoomRepository roomRepository;

    @GetMapping("/room")
    public List<Room> getAllData() {
        return roomRepository.findAll();
    }

    @GetMapping("/room/{id}")
    public Room getDataById(@PathVariable int id) {
        return roomRepository.findById(id);
    }

    @PostMapping("/room")
    @ResponseStatus(HttpStatus.CREATED)
    public Room addData(@RequestBody Room room) {
        return roomRepository.save(room);
    }

    @DeleteMapping("/room/{id}")
    public void deleteData(@PathVariable int id) {
        roomRepository.deleteById(id);
    }

    @PutMapping("/room/{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody Room room)
    {
        Optional<Room> rooms
                = Optional.ofNullable(
                roomRepository.findById(id));

        if (!rooms.isPresent())
            return ResponseEntity
                    .notFound()
                    .build();

        room.setId(id);

        roomRepository.save(room);

        return ResponseEntity
                .noContent()
                .build();
    }
}
