package id.co.nexsoft.house.model.building;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "floor")
    private int floor;

    @Column(name = "area")
    private int area;

    @ManyToOne
    @JoinColumn(name = "building_id")   
    @JsonBackReference 
    private Building building_id;

    @OneToMany(mappedBy = "room_id", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<ObjectCategory> categoryList;

    public Room() {}

    public Room(String name, int floor, int area, Building building_id) {
        this.name = name;
        this.floor = floor;
        this.area = area;
        this.building_id = building_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public Building getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(Building building_id) {
        this.building_id = building_id;
    }
}
