package id.co.nexsoft.house.repository.occupant;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.occupant.Occupant;

import java.util.List;

public interface OccupantRepository extends CrudRepository<Occupant, Integer> {
    Occupant findById(int id);
    List<Occupant> findAll();
    void deleteById(int id);
}
