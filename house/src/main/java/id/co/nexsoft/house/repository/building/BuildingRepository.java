package id.co.nexsoft.house.repository.building;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.building.Building;

public interface BuildingRepository extends CrudRepository<Building, Integer> {
    Building findById(int id);
    List<Building> findAll();
    void deleteById(int id);
}

