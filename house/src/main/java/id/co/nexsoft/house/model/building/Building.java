package id.co.nexsoft.house.model.building;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import id.co.nexsoft.house.model.House;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "building")
public class Building {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "house_id")
    @JsonBackReference
    private House house_id;

    @OneToMany(mappedBy = "building_id", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Room> roomList;

    public Building() {
    }

    public Building(String name, House house_id) {
        this.name = name;
        this.house_id = house_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public House getHouse_id() {
        return house_id;
    }

    public void setHouse_id(House house_id) {
        this.house_id = house_id;
    }
}
