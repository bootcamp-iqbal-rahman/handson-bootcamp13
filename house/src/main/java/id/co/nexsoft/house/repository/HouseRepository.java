package id.co.nexsoft.house.repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.House;
import java.util.List;


public interface HouseRepository extends CrudRepository<House, Integer> {
    House findById(int id);
    List<House> findAll();
    void deleteById(int id);
}
