package id.co.nexsoft.house.controller.occupant;

import id.co.nexsoft.house.model.occupant.Occupant;
import id.co.nexsoft.house.repository.occupant.OccupantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class OccupantController {

    @Autowired
    private OccupantRepository occupantRepository;

    @GetMapping("/occupant")
    public List<Occupant> getAllData() {
        return occupantRepository.findAll();
    }

    @GetMapping("/occupant/{id}")
    public Occupant getDataById(@PathVariable int id) {
        return occupantRepository.findById(id);
    }

    @PostMapping("/occupant")
    @ResponseStatus(HttpStatus.CREATED)
    public Occupant addData(@RequestBody Occupant occupant) {
        return occupantRepository.save(occupant);
    }

    @DeleteMapping("/occupant/{id}")
    public void deleteData(@PathVariable int id) {
        occupantRepository.deleteById(id);
    }

    @PutMapping("/occupant/{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody Occupant occupant) {
        Optional<Occupant> occupants = Optional.ofNullable(occupantRepository.findById(id));

        if (!occupants.isPresent())
            return ResponseEntity.notFound().build();

        occupant.setId(id);

        occupantRepository.save(occupant);

        return ResponseEntity.noContent().build();
    }
}
