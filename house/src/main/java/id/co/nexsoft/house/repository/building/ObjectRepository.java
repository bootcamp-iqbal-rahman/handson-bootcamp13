package id.co.nexsoft.house.repository.building;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.house.model.building.Object;

public interface ObjectRepository extends CrudRepository<Object, Integer> {
    Object findById(int id);
    List<Object> findAll();
    void deleteById(int id);
}
