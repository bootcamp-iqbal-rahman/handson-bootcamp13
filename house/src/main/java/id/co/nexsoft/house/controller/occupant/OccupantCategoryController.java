package id.co.nexsoft.house.controller.occupant;

import id.co.nexsoft.house.model.occupant.OccupantCategory;
import id.co.nexsoft.house.repository.occupant.OccupantCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class OccupantCategoryController {

    @Autowired
    private OccupantCategoryRepository occupantCategoryRepository;

    @GetMapping("/occupantcategory")
    public List<OccupantCategory> getAllData() {
        return occupantCategoryRepository.findAll();
    }

    @GetMapping("/occupantcategory/{id}")
    public OccupantCategory getDataById(@PathVariable int id) {
        return occupantCategoryRepository.findById(id);
    }

    @PostMapping("/occupantcategory")
    @ResponseStatus(HttpStatus.CREATED)
    public OccupantCategory addData(@RequestBody OccupantCategory occupantCategory) {
        return occupantCategoryRepository.save(occupantCategory);
    }

    @DeleteMapping("/occupantcategory/{id}")
    public void deleteData(@PathVariable int id) {
        occupantCategoryRepository.deleteById(id);
    }

    @PutMapping("/occupantcategory/{id}")
    public ResponseEntity<Object> putDataById(
            @PathVariable int id,
            @RequestBody OccupantCategory occupantCategory) {
        Optional<OccupantCategory> occupantCategories = Optional.ofNullable(occupantCategoryRepository.findById(id));

        if (!occupantCategories.isPresent())
            return ResponseEntity.notFound().build();

        occupantCategory.setId(id);

        occupantCategoryRepository.save(occupantCategory);

        return ResponseEntity.noContent().build();
    }
}
