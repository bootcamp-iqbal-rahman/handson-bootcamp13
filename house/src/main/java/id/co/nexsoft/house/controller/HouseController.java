package id.co.nexsoft.house.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PutMapping;

import id.co.nexsoft.house.model.House;
import id.co.nexsoft.house.repository.HouseRepository;

@RestController
public class HouseController {
    
    @Autowired
    private HouseRepository houseRepository;

    @GetMapping("/house")
    public List<House> getAllData() {
        return houseRepository.findAll();
    }

    @GetMapping("/house/{id}")
    public House getDataById(@PathVariable int id) {
        return houseRepository.findById(id);
    }

    @PostMapping("/house")
    @ResponseStatus(HttpStatus.CREATED)
    public House addData(@RequestBody House house) {
        return houseRepository.save(house);
    }

    @DeleteMapping("/house/{id}")
    public void deleteData(@PathVariable int id) {
        houseRepository.deleteById(id);
    }

    @PutMapping("/house/{id}")
    public ResponseEntity<Object> putDataById(
        @PathVariable int id, 
        @RequestBody House house) 
    {
        Optional<House> houses
            = Optional.ofNullable(
                houseRepository.findById(id));

        if (!houses.isPresent())
            return ResponseEntity
                .notFound()
                .build();

        house.setId(id);

        houseRepository.save(house);

        return ResponseEntity
            .noContent()
            .build();
    }
}
